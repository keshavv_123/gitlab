# frozen_string_literal: true

module QA
  module EE
    module Resource
      class WorkItemEpic < QA::Resource::Base
        include Support::Dates

        attribute :group do
          QA::Resource::Group.fabricate_via_api! do |group|
            group.path = "group-to-test-epic-work-items-#{SecureRandom.hex(8)}"
          end
        end

        attributes :id,
          :iid,
          :title,
          :description,
          :labels,
          :start_date_is_fixed,
          :start_date_fixed,
          :due_date_is_fixed,
          :due_date_fixed,
          :confidential,
          :author,
          :start_date,
          :due_date

        def initialize
          @title = "WI-Epic-#{SecureRandom.hex(8)}"
          @description = "This is a work item epic description."
          @confidential = false
          @start_date_is_fixed = false
          @due_date_is_fixed = false
        end

        def fabricate!
          raise NotImplementedError
        end

        def gid
          "gid://gitlab/WorkItem/#{id}"
        end

        # Work item epic attributes
        #
        # @return [String]
        def gql_attributes
          @gql_attributes ||= <<~GQL
            author {
              id
            }
            confidential
            createdAt
            updatedAt
            closedAt
            description
            id
            iid
            namespace {
              id
            }
            state
            title
            widgets {
              ... on WorkItemWidgetRolledupDates
              {
                type
                dueDate
                dueDateFixed
                dueDateIsFixed
                startDate
                startDateFixed
                startDateIsFixed
              }
              ... on WorkItemWidgetLabels
              {
                type
                labels
                {
                  nodes
                  {
                    title
                  }
                }
              }
              ... on WorkItemWidgetAwardEmoji
              {
                type
                upvotes
                downvotes
              }
              ... on WorkItemWidgetColor
              {
                type
                color
                textColor
              }
            }
            workItemType {
              name
              id
            }
          GQL
        end

        # Path for fetching work item epic
        #
        # @return [String]
        def api_get_path
          "/graphql"
        end

        # Fetch work item epic
        #
        # @return [Hash]
        def api_get
          process_api_response(
            api_post_to(
              api_get_path,
              <<~GQL
                query {
                  workItem(id: "#{gid}") {
                    #{gql_attributes}
                  }
                }
              GQL
            )
          )
        end

        # Path to create work item epic
        #
        # @return [String]
        def api_post_path
          "/graphql"
        end

        # Return subset of variable date fields for comparing work item epics with legacy epics
        # Can be removed after migration to work item epics is complete
        #
        # @return [Hash]
        def epic_dates
          reload! if api_response.nil?

          api_resource.slice(
            :created_at,
            :updated_at,
            :closed_at
          )
        end

        # Return author field for comparing work item epics with legacy epics
        # Can be removed after migration to work item epics is complete
        #
        # @return [Hash]
        def epic_author
          reload! if api_response.nil?

          api_resource[:author][:id] = api_resource.dig(:author, :id).split('/').last.to_i

          api_resource.slice(
            :author
          )
        end

        # Return iid for comparing work item epics with legacy epics
        # Can be removed after migration to work item epics is complete
        #
        # @return [Hash]
        def epic_iid
          reload! if api_response.nil?

          api_resource[:iid] = api_resource[:iid].to_i

          api_resource.slice(:iid)
        end

        # Return namespace id for comparing work item epics with legacy epics
        # Can be removed after migration to work item epics is complete
        #
        # @return [Hash]
        def epic_namespace_id
          reload! if api_response.nil?

          api_resource[:group_id] = api_resource.dig(:namespace, :id).split('/').last.to_i

          api_resource.slice(:group_id)
        end

        protected

        # rubocop:disable Metrics/AbcSize -- temp comparable for epic to work items migration
        # Return subset of fields for comparing work item epics to legacy epics
        #
        # @return [Hash]
        def comparable
          reload! unless api_response.nil?

          api_resource[:state] = convert_graphql_state_to_legacy_state(api_resource[:state])
          api_resource[:labels] = get_widget('LABELS')&.dig(:labels, :nodes)
          api_resource[:upvotes] = get_widget('AWARD_EMOJI')&.dig(:upvotes)
          api_resource[:downvotes] = get_widget('AWARD_EMOJI')&.dig(:downvotes)
          api_resource[:start_date] = get_widget('ROLLEDUP_DATES')&.dig(:start_date)
          api_resource[:due_date] = get_widget('ROLLEDUP_DATES')&.dig(:due_date)
          api_resource[:start_date_is_fixed] = get_widget('ROLLEDUP_DATES')&.dig(:start_date_is_fixed)
          api_resource[:start_date_fixed] = get_widget('ROLLEDUP_DATES')&.dig(:start_date_fixed)
          api_resource[:due_date_is_fixed] = get_widget('ROLLEDUP_DATES')&.dig(:due_date_is_fixed)
          api_resource[:due_date_fixed] = get_widget('ROLLEDUP_DATES')&.dig(:due_date_fixed)
          api_resource[:color] = get_widget('COLOR')&.dig(:color)
          api_resource[:text_color] = get_widget('COLOR')&.dig(:text_color)

          api_resource.slice(
            :title,
            :description,
            :state,
            :start_date,
            :due_date,
            :start_date_is_fixed,
            :start_date_fixed,
            :due_date_is_fixed,
            :due_date_fixed,
            :confidential,
            :labels,
            :upvotes,
            :downvotes,
            :color,
            :text_color
          )
        end
        # rubocop:enable Metrics/AbcSize

        def convert_graphql_state_to_legacy_state(state)
          case state
          when 'OPEN'
            'opened'
          when 'CLOSE'
            'closed'
          end
        end

        def get_widget(type)
          api_resource[:widgets].find { |widget| widget[:type] == type }
        end
      end
    end
  end
end

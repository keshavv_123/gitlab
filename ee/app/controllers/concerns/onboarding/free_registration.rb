# frozen_string_literal: true

module Onboarding
  class FreeRegistration
    PRODUCT_INTERACTION = 'Personal SaaS Registration'
    private_constant :PRODUCT_INTERACTION
    TRACKING_LABEL = 'free_registration'
    private_constant :TRACKING_LABEL

    # string methods

    def self.tracking_label
      TRACKING_LABEL
    end

    def self.product_interaction
      PRODUCT_INTERACTION
    end

    # internalization methods

    def self.welcome_submit_button_text
      _('Continue')
    end

    def self.setup_for_company_label_text
      _('Who will be using GitLab?')
    end

    # predicate methods

    def self.redirect_to_company_form?
      false
    end

    def self.eligible_for_iterable_trigger?
      true
    end

    def self.show_joining_project?
      true
    end

    def self.show_opt_in_to_email?
      true
    end

    def self.hide_setup_for_company_field?
      false
    end

    def self.pre_parsed_email_opt_in?
      false
    end

    def self.apply_trial?
      false
    end

    def self.read_from_stored_user_location?
      false
    end

    def self.preserve_stored_location?
      false
    end
  end
end
